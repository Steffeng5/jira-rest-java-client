/*
 * Copyright (C) 2010 Atlassian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.jira.rest.client.internal.json;

import com.atlassian.jira.rest.client.api.domain.User;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.net.URI;

public class GroupMemberJsonParser implements JsonObjectParser<Iterable<User>> {
    private final UserJsonParser userJsonParser = new UserJsonParser();

    @Override
    public Iterable<User> parse(JSONObject json) throws JSONException {
//        final URI self = JsonParseUtil.getSelfUri(json);
//        final URI nextPage; //To be implemented?
//        final long maxResults = json.getLong("maxResults");
//        final long startAt = json.getLong("startAt");
//        final long total = json.getLong("total");
//        final boolean isLast = json.getBoolean("isLast");
//
        final JSONArray usersJsonArray = json.getJSONArray("values");

        return JsonParseUtil.parseJsonArray(usersJsonArray, userJsonParser);
    }
}
