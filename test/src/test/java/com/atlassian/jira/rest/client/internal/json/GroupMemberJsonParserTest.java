/*
 * Copyright (C) 2010 Atlassian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.jira.rest.client.internal.json;

import com.atlassian.jira.rest.client.api.domain.User;
import org.hamcrest.collection.IsEmptyIterable;
import org.junit.Assert;
import org.junit.Test;

// Ignore "May produce NPE" warnings, as we know what we are doing in tests
@SuppressWarnings("ConstantConditions")
public class GroupMemberJsonParserTest {
    private final GroupMemberJsonParser parser = new GroupMemberJsonParser();

    @Test
    public void testParse() throws Exception {
        Iterable<User> users = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/group/member/valid.json"));
        Assert.assertTrue(users != null);

        for (User user : users) {
            Assert.assertTrue(user.getEmailAddress().contains("atlassian"));
            Assert.assertTrue(user.getName().length() > 3);
        }
    }
}
